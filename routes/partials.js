/**
 * The partials methods for routers
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

 /**
  * Libs
  */

/**
 * Controllers
 */
const errorController = require('../controllers/error');

/*********************************************/
/* LOGIN AND CHECKING THAT USER IS LOGGED IN */
/*********************************************/

// check that user is logged in
module.exports.loginControl = (req, res, next) => {
    if (req.session.user) {
        next();
    } else {
        //console.log(res);
        //console.log(req);
        if (res.locals.currentPath.includes('admin')) {
            res.redirect('/admin/login');
        } else if (res.locals.currentPath.includes('user-site')) {
            res.redirect('/login');
        } else {
            res.redirect('/');
        }
    }
};

module.exports.loggedIn = (req, res, next) => {
    if (req.session.user != undefined) {
        if (req.session.user.permissions.includes('masterAdmin') || req.session.user.permissions.includes('admin')) {
            return res.redirect('/admin/dashboard');
        } else {
            return res.redirect('/');
        }
    } else {
        next();
    }
};

module.exports.filterNonAdmin = (req, res, next) => {
    if (req.session.user != undefined) {
        //console.log(req.session.user.permissions.includes('admin'));
        if (req.session.user.permissions.includes('admin')) {
            next();
        } else if (req.session.user.permissions.includes('masterAdmin')) {
            next();
        } else {
            errorController.error403(req, res);
        }
    } else {
        errorController.error403(req, res);
    }
};

module.exports.filterByPermissions = (req, res, next, permission, shortAnswer) => {
    setTimeout(function () {
        if (!permission) {
            permission = req.originalUrl.split('/')[2];
        }
        //console.log(permission);
        //console.log(req.session.user.permissions);
        if (req.session.user.permissions.includes('masterAdmin')) {
            next();
        } else if (!req.session.user.permissions.includes(permission)) {
            if (shortAnswer) {
                return res.send('access-denied');
            }
            errorController.error403admin(req, res);
        } else {
            next();
        }
    }, 200);
};