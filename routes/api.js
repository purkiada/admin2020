/**
 * The API router of the app
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

/**
 * Express router API
 */
const router = require('express').Router();

/**
 * Libraries
 */
const moment = require('moment');
moment.locale('cs');
const numberFormat = require('../libs/numberFormat');
const osloveni = require('../libs/osloveni');

/**
 * Controllers
 */
const errorController = require('../controllers/error');
const feedbackController = require('../controllers/feedback');
const reservationController = require('../controllers/reservation');
const pageController = require('../controllers/page');
const userController = require('../controllers/user');
const studentController = require('../controllers/student');
const villageController = require('../controllers/village');
const importController = require('../controllers/import');
const schoolController = require('../controllers/school');
/**
 * Partials methods
 */
const partials = require('./partials');

/**
 ******** Routes
 */

// set local variables
router.all('/*', (req, res, next) => {
    res.locals = {
        currentPath: req.originalUrl,
        moment,
        numberFormat,
        osloveni
    };

    // if (req.session.year == undefined) {
    const Year = require('../models/Year');
    Year.findOne({
        status: 'active',
    }, (err, year) => {
        if (err) {
            return console.error(err);
        }

        if (year == null) {
            let msg = `Error! I couldn't find any year in database.`;
            console.error(msg);
            return res.send(msg)
        }

        req.session.year = year;

        // Move to the next route
        next();
    });
    // } else {
    //     next();
    // }

});


/**
 * Homepage
 */
router.get('/', (req, res) => {
    res.send('Hi my dear friend!');
});

/**
 * Import
 */
// Should be in comment
/*router.get('/schools/import-from-static', (req, res) => {
    importController.tmp(req, res);
});*/

/**
 * Students
 */
router.get('/student/list', partials.loginControl, partials.filterNonAdmin, (req, res) => {
    studentController.list(req, res);
});

router.get('/students/count', (req, res) => {
    studentController.count(req, res);
});

router.post('/student/new', (req, res, next) => {
    studentController.canICreateNew(req, res, next);
}, (req, res) => {
    studentController.new(req, res);
});

router.get('/student/confirm', (req, res) => {
    studentController.confirm(req, res);
});

/**
 * Village
 */
router.get('/village/list', (req, res) => {
    villageController.list(req, res);
});

/**
 * School
 */
router.get('/school/list', (req, res) => {
    schoolController.list(req, res);
});

router.all('/school/find', (req, res) => {
    schoolController.find(req, res);
});


router.get('*', (req, res) => {
    return res.status(404).send('404');
});

module.exports = router;