/**
 * The entry router of the app
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

/**
 * Express router API
 */
const router = require('express').Router();

/**
 * Libraries
 */
const moment = require('moment');
moment.locale('cs');
const numberFormat = require('../libs/numberFormat');
const osloveni = require('../libs/osloveni');

/**
 * Models
 */
const Year = require('../models/Year');

/**
 * Controllers
 */
const errorController = require('../controllers/error');
const feedbackController = require('../controllers/feedback');
const reservationController = require('../controllers/reservation');
const pageController = require('../controllers/page');
const userController = require('../controllers/user');
const studentController = require('../controllers/student');
const importController = require('../controllers/import');

/**
 * Partials methods
 */
const partials = require('./partials');

/**
 ******** Routes
 */

// set local variables
router.all('/*', (req, res, next) => {
    res.locals = {
        currentPath: req.originalUrl,
        moment,
        numberFormat,
        osloveni
    };
    let goNext = true;

    if (req.session.year == undefined) {
        Year.findOne({
            status: 'active',
        })
            .exec((err, year) => {
                if (err) {
                    return console.error(err);
                }

                if (year != null) {
                    req.session.year = year;
                } else {
                    goNext = false;
                    let msg = `Error! I couldn't find any year in database.`;
                    console.error(msg);
                    //return res.send(msg)
                }
            });
    }

    // Move to the next route
    if (goNext) {
        next();
    }
    /*
        // if (req.session.year == undefined) {
            const Year = require('../models/Year');
            Year.findOne({
                status: 'active',
            }, (err, year) => {
                if (err) {
                    return console.error(err);
                }
    
                if (year == null) {
                    let msg = `Error! I couldn't find any year in database.`;
                    console.error(msg);
                    return res.send(msg)
                }
    
                req.session.year = year;
    
                // Move to the next route
                next();
            });
        // } else {
        //     next();
        // }
        */

});


/**
 * Homepage
 */
router.get('/', (req, res) => {
    return res.redirect('/admin/login');
    pageController.homepage(req, res);
});

/**
 * Reservations
 * /
router.get('/reservation', (req, res) => {
    pageController.reservation(req, res);
});

router.post('/reservations/new', (req, res) => {
    if (moment().diff(req.session.year.reservations.end) >= 0) {
        console.log(`Someone tried to create new reservation ${req.body.email ? req.body.email : ''}!\n${req.body}`);
        return res.redirect('/reservation?err=closed-by-time');
    } else {
        reservationController.new(req, res);
    }
});*/

/**
 * Applications
 * /
router.get('/application', (req, res) => {
    pageController.application(req, res);
});

router.post('/application/new', (req, res) => {
    studentController.new(req, res);
});*/

/**
 * Feedbacks
 */
// router.get('/feedback', (req, res) => {
//     pageController.feedback(req, res);
// });

// router.post('/feedbacks/new', (req, res) => {
//     feedbackController.new(req, res);
// });

/**
 * Poster
 * /
router.get('/poster', (req, res) => {
    pageController.poster(req, res);
});*/

/**
 * Login pages
 */
router.get('/login', (req, res) => {
    res.redirect('/admin/login');
});

router.get('/register', (req, res) => {
    res.redirect('/admin/register');
});

router.get('/forgot-password', (req, res) => {
    res.redirect('/admin/forgot-password');
});

router.all('/logout', (req, res) => {
    userController.logout(req, res);
});

router.all('/d', (req, res) => {
    req.session.destroy();
    res.redirect('/?status=destroy-ok');
});

// router.get('/setup', (req, res) => {
//     new Year({
//         name: moment().format('YYYY'),
//         status: 'active',
//     }).save((err, year) => {
//         if (err) {
//             console.error(err);
//             return res.send(err);
//         }
//         return res.send('ok');
//     });
// });

router.get('*', (req, res) => {
    errorController.error404(req, res);
});

module.exports = router;