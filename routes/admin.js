/**
 * The admin router of the app
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

/**
 * Express router API
 */
const router = require('express').Router();

/**
 * Libraries
 */
const moment = require('moment');
moment.locale('cs');
const numberFormat = require('../libs/numberFormat');
const osloveni = require('../libs/osloveni');

/**
 * Controllers
 */
const adminPageController = require('../controllers/admin/page');
const emailController = require('../controllers/admin/email');
const feedbackController = require('../controllers/feedback');
const reservationController = require('../controllers/reservation');
const userController = require('../controllers/user');
const yearController = require('../controllers/year');
const studentController = require('../controllers/student');

/**
 * Partials methods
 */
const partials = require('./partials');

/**
 * Routes
 */

// set local variables
router.all('/*', (req, res, next) => {
    res.locals = {
        currentPath: req.originalUrl,
        moment,
        numberFormat,
        osloveni,
    };
    let goNext = true;

    if (req.session.year == undefined) {
        const Year = require('../models/Year');
        Year.findOne({
            status: 'active',
        })
            .exec((err, year) => {
                if (err) {
                    return console.error(err);
                }
                
                if (year != null) {
                    req.session.year = year;
                } else {
                    goNext = false;
                    let msg = `Error! I couldn't find any year in database.`;
                    console.error(msg);
                    //return res.send(msg)
                }
            });
    }

    // Move to the next route
    if (goNext) {
        next();
    }
});

// Redirect from /admin to the login page or to dashboard
router.get('/', partials.loggedIn, (req, res) => {
    res.redirect('/admin/dashboard');
});

// Display login page
router.get('/login', partials.loggedIn, (req, res) => {
    adminPageController.login(req, res);
});

// Login the user
router.post('/login', partials.loggedIn, (req, res) => {
    userController.login(req, res);
});

router.get('/register', partials.loggedIn, (req, res) => {
    adminPageController.register(req, res);
});

router.post('/register', partials.loggedIn, (req, res) => {
    userController.register(req, res);
});

router.get('/forgot-password', partials.loggedIn, (req, res) => {
    adminPageController.forgotPassword(req, res);
});

router.post('/forgot-password', partials.loggedIn, (req, res) => {
    userController.resetPassword(req, res);
});

/**
 * This row block access without logging in
 */
router.all('/*', partials.loginControl);

// This row block access without admin permissions
router.all('/*', partials.filterNonAdmin);

/**
 * Dashboard
 */

router.get('/dashboard/', (req, res) => {
    adminPageController.dashboard(req, res);
});

/**
 * Students
 */
router.get('/students/list', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'students-list');
}, (req, res) => {
    adminPageController.listStudents(req, res);
});

router.get('/students/edit', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'students-edit');
}, (req, res) => {
    adminPageController.editStudent(req, res);
});

router.get('/students/new', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'students-new');
}, (req, res) => {
    adminPageController.newStudent(req, res);
});

router.post('/students/delete', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'students-delete');
}, (req, res) => {
    studentController.delete(req, res);
});

router.post('/students/edit', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'students-edit');
}, (req, res) => {
    studentController.edit(req, res);
});

router.get('/students/detail', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'students-list');
}, (req, res) => {
    adminPageController.detailStudent(req, res);
});

router.post('/students/change-status', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'students-change-status');
}, (req, res) => {
    studentController.changeStatus(req, res);
});

/**
 * Reservations
 */
router.get('/reservations', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'reservations-list');
}, (req, res) => {
    res.redirect('/admin/reservations/list');
});

router.get('/reservations/list', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'reservations-list');
}, (req, res) => {
    adminPageController.listReservations(req, res);
});

router.get('/reservations/new', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'reservations-new');
}, (req, res) => {
    adminPageController.newReservation(req, res);
});

router.post('/reservations/new', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'reservations-new');
}, (req, res) => {
    reservationController.new(req, res);
});

router.get('/reservations/edit', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'reservations-edit');
}, (req, res) => {
    adminPageController.editReservation(req, res);
});

router.post('/reservations/edit', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'reservations-edit');
}, (req, res) => {
    reservationController.edit(req, res);
});

router.post('/reservations/delete', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'reservations-delete');
}, (req, res) => {
    reservationController.delete(req, res);
});

router.post('/reservations/change-status', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'reservations-change-status');
}, (req, res) => {
    reservationController.changeStatus(req, res);
});

router.post('/reservations/send-email', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'email');
}, (req, res) => {
    reservationController.sendEmail(req, res);
});


/**
 * Feedbacks
 */
router.get('/feedbacks', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'feedbacks-list');
}, (req, res) => {
    res.redirect('/admin/feedbacks/list');
});

router.get('/feedbacks/list', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'feedbacks-list');
}, (req, res) => {
    adminPageController.listFeedbacks(req, res);
});

router.post('/feedbacks/delete', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'feedbacks-delete');
}, (req, res) => {
    feedbackController.delete(req, res);
});

/**
 * Years
 */
router.post('/year', (req, res) => {
    yearController.switch(req, res);
});

router.get('/years', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'years-list');
}, (req, res) => {
    res.redirect('/admin/years/list');
});

router.get('/years/list', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'years-list');
}, (req, res) => {
    adminPageController.listYears(req, res);
});

router.get('/years/new', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'years-new');
}, (req, res) => {
    adminPageController.newYear(req, res);
});

router.post('/years/new', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'years-new');
}, (req, res) => {
    yearController.new(req, res);
});

router.get('/years/edit', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'years-edit');
}, (req, res) => {
    adminPageController.editYear(req, res);
});

router.post('/years/edit', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'years-edit');
}, (req, res) => {
    yearController.edit(req, res);
});

router.post('/years/delete', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'years-delete');
}, (req, res) => {
    yearController.delete(req, res);
});

router.post('/years/change-status', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'years-change-status');
}, (req, res) => {
    yearController.changeStatus(req, res);
});

/**
 * Users
 */
router.get('/users', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'users-list');
}, (req, res) => {
    res.redirect('/admin/users/list');
});

router.get('/users/list', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'users-list');
}, (req, res) => {
    adminPageController.listUsers(req, res);
});

router.get('/users/new', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'users-new');
}, (req, res) => {
    adminPageController.newUser(req, res);
});

router.post('/users/new', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'users-new');
}, (req, res) => {
    userController.register(req, res);
});

router.get('/users/edit', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'users-edit');
}, (req, res) => {
    adminPageController.editUser(req, res);
});

router.post('/users/edit', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'users-edit');
}, (req, res) => {
    userController.edit(req, res);
});

router.post('/users/set-new-password', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'users-change-password');
}, (req, res) => {
    userController.setNewPassword(req, res);
});

router.post('/users/delete', (req, res, next) => {
    partials.filterByPermissions(req, res, next, 'users-delete');
}, (req, res) => {
    userController.delete(req, res);
});

/**
 * Profile
 */
 router.get('/profile', (req, res) => {
     adminPageController.profile(req, res);
 });

/**
 * Emails
 */
router.get('/emails/write', partials.filterByPermissions, (req, res) => {
    adminPageController.writeEmail(req, res);
});

router.post('/emails/write', partials.filterByPermissions, (req, res) => {
    emailController.sendEmail(req, res);
});

/**
 * Temp pages
 * - for tests like error pages
 */
router.all('/ad', (req, res) => {
    adminPageController.error403(req, res);
});

router.all('/testik', (req, res) => {
    res.send(req.body);
});

/**
 * Logout
 */
router.get('/logout', (req, res) => {
    userController.logout(req, res);
});

/**
 * Not found the rerquested path
 */
router.all('*', (req, res) => {
    // errorController.error404(req, res);
    adminPageController.error404(req, res);
});

/**
 * Export the router
 */
module.exports = router;