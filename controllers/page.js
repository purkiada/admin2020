/**
 * Page controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */

/**
 * Models
 */

module.exports.homepage = (req, res) => {
    res.render('homepage', {req, res});
};

module.exports.reservation = (req, res) => {
    res.render('reservation', {req, res});
};

module.exports.application = (req, res) => {
    res.render('application', {req, res});
};

module.exports.feedback = (req, res) => {
    res.render('feedback', {req, res});
};

module.exports.poster = (req, res) => {
    res.render('poster', {req, res, active: 'poster'});
};