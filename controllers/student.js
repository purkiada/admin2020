/**
 * Student controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const moment = require('moment');
moment.locale('cs');
const nodemailer = require("nodemailer");
const validator = require('validator');
const randomstring = require('randomstring');

/**
 * Models
 */
const User = require('../models/User');
const Student = require('../models/Student');
const Year = require('../models/Year');

module.exports.new = (req, res) => {
    // save the object to the database, handle the errors
    let email = req.body.email.trim().toLowerCase();
    if (!validator.isEmail(email)) {
        return res.send('not-email');
        return res.redirect(`/student/?err=this-is-not-email${req.body.firstname ? `&firstname=${req.body.firstname}` : ''}${req.body.lastname ? `&lastname=${req.body.lastname}` : ''}${req.body.goOutTime ? `&goOutTim=${req.body.goOutTime}` : ''}${req.body.countOfChildren ? `&countOfChildren=${req.body.countOfChildren}` : ''}${req.body.countOfAdults ? `&countOfAdults=${req.body.countOfAdults}` : ''}#student`);
    }
    req.session.email = email;

    // find the student by email
    Student.findOne({
        year: req.session.year,
        email: req.session.email,
    }, (err, student) => {
        if (err)
            return console.error(err);

        if (student && student.email === email) {
            // student with inputed email found
            return res.send('email-exists');
            return res.redirect(`/student/?err=emailAlreadyExists${req.body.firstname ? `&firstname=${req.body.firstname}` : ''}${req.body.lastname ? `&lastname=${req.body.lastname}` : ''}${req.body.goOutTime ? `&goOutTim=${req.body.goOutTime}` : ''}${req.body.countOfChildren ? `&countOfChildren=${req.body.countOfChildren}` : ''}${req.body.countOfAdults ? `&countOfAdults=${req.body.countOfAdults}` : ''}#student`);
        }

        // No student with inputed email found

        // Create new student
        new Student({
            name: {
                first: req.body.firstname,
                last: req.body.lastname,
            },
            email,
            date: moment(),
            key: randomstring.generate(),
            ip: req.headers['x-forwarded-for'],
            school: req.body.school,
            year: req.session.year._id,
        }).save((err, student) => {
            if (err) {
                return console.error(err);
            }

            // Create a SMTP transporter object
            let transporter = nodemailer.createTransport(global.CONFIG.nodemailer.settings);

            // Message object
            let message = {
                from: CONFIG.nodemailer.sender,
                to: student.name.first + ' ' + student.name.last + ' <' + student.email + '>',
                subject: `Potvrďte prosím Vaši přihlášku 📆`,
                text: `Dobrý den,\n\nprosíme o potvrzení Vaší přihlášky na Purkiádu s těmito údajemi:\n
            ID: ${student._id}
            Křestní jméno: ${student.name.first}
            Příjmení: ${student.name.last}
            Email: ${student.email}\n\n\nPokud vše souhlasí, tak Vás prosím o potvrzení zde: https://purkiada.cz/confirm-student.html?id=${student._id}\n\nS přáním hezkého dne,\nPořadatelé Purkiády`,
                //html: '<h1>Dobrý den,</h1><p>zde je vaše heslo <b>' + newPassword + '</b>!</p><p>Nyní se můžete přihlásit s tímto heslem.</p><br><p>S přáním hezkého dne,<br/>Pořadatelé Pohádkového lesa v Rudici</p>'
                //html: fs.readFileSync('../mail/forgot-password.html');
            };

            transporter.sendMail(message, (err, info, response) => {
                if (err) {
                    console.error('Error occurred. ' + err.message);
                    if (req.originalUrl != '/api/student/new') {
                        return res.redirect(`${req.originalUrl}?status=${err.name}`);
                        //res.redirect('/?status=student-ok#student');
                        //res.redirect('/admin/students/?status=ok');
                    } else {
                        return res.send(`${err.name}`);
                        return res.redirect(`/student/?status=${err.name}#student`);
                    }
                    return process.exit(1);
                }
                // console.log( info);
                // console.log(response);
                //console.log(`Successfull student!\n${req.body}`);
                console.log(`${moment().format('YYYY-MM-DD HH:mm:ss')} Successfully created new student! ${student.email}`);

                return res.send('ok');
                // redirect back (after the save is done)
                if (req.originalUrl != '/students/new') {
                    if (req.originalUrl.includes('new')) {
                        req.originalUrl = req.originalUrl.split('new')[0];
                    }
                    return res.redirect(`${req.originalUrl}?status=ok`);
                    //res.redirect('/?status=student-ok#student');
                    //res.redirect('/admin/students/?status=ok');
                } else {
                    return res.redirect('/student/?status=ok');
                }
            });
        });
    });
};

module.exports.confirm = (req, res) => {
    if (!req.query.id) {
        return res.send('not-sent-id');
    }
    Student.findOneAndUpdate({
        _id: req.query.id,
        year: req.session.year._id,
    }, {
        confirm: true,
        username: randomstring.generate(),
        password: randomstring.generate(),
    }, {
        new: true,
    }).exec((err, student) => {
        if (err) {
            res.send(err);
            return console.error(err);
        }
        if (student === undefined) {
            return res.send('not-find-student');
        }

        // Create a SMTP transporter object
        let transporter = nodemailer.createTransport(global.CONFIG.nodemailer.settings);

        // Message object
        let message = {
            from: CONFIG.nodemailer.sender,
            to: student.name.first + ' ' + student.name.last + ' <' + student.email + '>',
            subject: `Vaše potvrzení přihlášky ✅`,
            text: `Dobrý den,\n\nteď už s Vámi počítáme, zde je potvrzení Vaší přihlášky:\n\tID: ${student._id}\n\tKřestní jméno: ${student.name.first}\n\tPříjmení: ${student.name.last}\n\tEmail: ${student.email}\n\n\nS přáním hezkého dne,\nPořadatelé Purkiády`,
        };

        transporter.sendMail(message, (err, info, response) => {
            if (err) {
                console.error('Error occurred. ' + err.message);
                if (req.originalUrl != '/api/student/new') {
                    return res.redirect(`${req.originalUrl}?status=${err.name}`);
                    //res.redirect('/?status=student-ok#student');
                    //res.redirect('/admin/students/?status=ok');
                } else {
                    return res.send(`${err.name}`);
                    return res.redirect(`/student/?status=${err.name}#student`);
                }
                return process.exit(1);
            }
            // console.log( info);
            // console.log(response);
            //console.log(`Successfull student!\n${req.body}`);
            console.log(`${moment().format('YYYY-MM-DD HH:mm:ss')} Successfully confirmed student! ${student.email}`);

            return res.send('ok');
            // redirect back (after the save is done)
            if (req.originalUrl != '/students/new') {
                if (req.originalUrl.includes('new')) {
                    req.originalUrl = req.originalUrl.split('new')[0];
                }
                return res.redirect(`${req.originalUrl}?status=ok`);
                //res.redirect('/?status=student-ok#student');
                //res.redirect('/admin/students/?status=ok');
            } else {
                return res.redirect('/student/?status=ok');
            }
        });
    });
};

module.exports.cancel = (req, res) => {
    if (!req.query.id) {
        return res.send('not-sent-id');
    }
    Student.findOneAndUpdate({
        _id: req.query.id,
        year: req.session.year._id,
    }, {
        confirm: false,
    }, {
        new: true,
    }).exec((err, student) => {
        if (err) {
            res.send(err);
            return console.error(err);
        }
        if (student === undefined) {
            return res.send('not-find-student');
        }

        // Create a SMTP transporter object
        let transporter = nodemailer.createTransport(global.CONFIG.nodemailer.settings);

        // Message object
        let message = {
            from: CONFIG.nodemailer.sender,
            to: student.name.first + ' ' + student.name.last + ' <' + student.email + '>',
            subject: `Vaše přihláška byla zrušena ❌`,
            text: `Dobrý den,\n\nteď už s Vámi bohužel nepočítáme, protože někdo v naší administraci zrušil Vaši přihlášku, pokud to je omyl, tak se nám odpovězte na tento email.\n\nS přáním hezkého dne,\nPořadatelé Purkiády`,
        };

        transporter.sendMail(message, (err, info, response) => {
            if (err) {
                console.error('Error occurred. ' + err.message);
                if (req.originalUrl != '/api/student/new') {
                    return res.redirect(`${req.originalUrl}?status=${err.name}`);
                    //res.redirect('/?status=student-ok#student');
                    //res.redirect('/admin/students/?status=ok');
                } else {
                    return res.send(`${err.name}`);
                    return res.redirect(`/student/?status=${err.name}#student`);
                }
                return process.exit(1);
            }
            // console.log( info);
            // console.log(response);
            console.log(`${moment().format('YYYY-MM-DD HH:mm:ss')} Successfully confirmed student! ${student.email}`);

            return res.send('ok');
        });
    });
};

module.exports.changeStatus = (req, res) => {
    if (req.body.id == undefined) {
        return res.send('not-sent-id');
    } else if (req.body.studentStatus == undefined) {
        return res.send('not-sent-status');
    }

    switch (req.body.studentStatus) {
        case 'confirm':
            req.query.id = req.body.id;
            return this.confirm(req, res);
            break;

        case 'cancel':
            req.query.id = req.body.id;
            return this.cancel(req, res);
            break;

        default:
            return res.send('wtf');
            break;
    }
};

module.exports.edit = (req, res) => {
    if (req.body.id == undefined) {
        return res.send('not-sent-id');
    }
    Student.findOneAndUpdate({
        _id: req.body.id,
    }, {
        name: {
            first: req.body.firstname,
            last: req.body.lastname,
        },
        email: req.body.email,
        description: req.body.description,
    }, {
        new: true,
    }, (err, student) => {
        if (err) {
            res.send(err);
            return console.error(err);
        }

        if (req.body.sendEmail) {
            // Create a SMTP transporter object
            let transporter = nodemailer.createTransport(global.CONFIG.nodemailer.settings);

            // Message object
            let message = {
                from: CONFIG.nodemailer.sender,
                to: student.name.first + ' ' + student.name.last + ' <' + student.email + '>',
                subject: `Vaše upravená přihláška ✅`,
                text: `Dobrý den,\n\nněkdo Vám upravil Vaši přihlášku v naší administraci, nejpravděpodobněji na Vaši žádost.\nZde je rekapitulace Vaší upravené přihlášky:\n\tID: ${student._id}\n\tKřestní jméno: ${student.name.first}\n\tPříjmení: ${student.name.last}\n\tEmail: ${student.email}\n\n\nS přáním hezkého dne,\nPořadatelé Purkiády`,
            };

            transporter.sendMail(message, (err, info, response) => {
                if (err) {
                    console.error('Error occurred. ' + err.message);
                    if (req.originalUrl != '/api/student/new') {
                        return res.redirect(`${req.originalUrl}?status=${err.name}`);
                        //res.redirect('/?status=student-ok#student');
                        //res.redirect('/admin/students/?status=ok');
                    } else {
                        return res.send(`${err.name}`);
                        return res.redirect(`/student/?status=${err.name}#student`);
                    }
                    return process.exit(1);
                }
                return res.redirect('/admin/students/list?status=edit-ok');
                return res.send('ok');
            });
        } else {
            return res.send('ok');
        }
    });
};

module.exports.delete = (req, res) => {
    if (!req.body.id) {
        res.send('not-send-id');
    } else {
        Student.findByIdAndDelete(req.body.id, (err) => {
            if (err)
                console.error(err);
            res.send('ok');
        });
    }
};

module.exports.deleteAll = (req, res) => {
    Student.deleteMany({}, (err) => {
        if (err) {
            return console.error(err);
        } else {
            res.send('ok');
        }
    });
};

module.exports.list = (req, res) => {
    Student.find({
        year: req.session.year._id,
    })
        .populate('village')
        .populate('school')
        .exec((err, students) => {
            if (err) {
                return console.error(err);
            }
            res.send(students);
        })
};

module.exports.count = (req, res) => {
    Student.countDocuments({
        year: req.session.year._id,
    }, (err, countOfStudents) => {
        if (err) {
            res.send('err');
            return console.error(err);
        }
        return res.send(JSON.stringify(countOfStudents));
    });
};

module.exports.canICreateNew = (req, res, next) => {
    Student.countDocuments({
        year: req.session.year._id,
    }, (err, countOfStudents) => {
        if (err) {
            res.send('err');
            return console.error(err);
        }
        if (moment().diff(req.session.year.reservations.end) >=0 ) {
            return res.send('timed-out');
        } else if (countOfStudents < 100) {
            next();
        } else {
            return res.send('full');
        }
    });
};