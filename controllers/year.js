/**
 * Year controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const moment = require('moment');
moment.locale('cs');

/**
 * Models
 */
const Year = require('../models/Year');
const User = require('../models/User');

module.exports.new = (req, res) => {
   new Year({
      name: req.body.name,
      author: req.session.user ? req.session.user._id : null,
      description: req.body.description,
      reservations: {
         start: req.body.reservationsStart,
         end: req.body.reservationsEnd,
      },
      created: moment(),
   }).save((err, year) => {
      if (err) {
         return console.error(err);
      }

      User.findByIdAndUpdate(req.session.user._id, {
         $push: {
            years: year._id,
         }
      }, (err) => {
         if (err) {
            return console.error(err);
         }

         return res.redirect('/admin/years/list?status=new-ok');
      });
   });
};

module.exports.delete = (req, res) => {
   if (req.body.id == undefined) {
      return res.send('not-send-year-id');
   }
   Year.findByIdAndRemove(req.body.id, (err, year) => {
      if (err) {
         return console.error(err);
      }

      User.findByIdAndUpdate(req.session.user._id,
         {
            $pull: {
               years: year._id,
            }
         }, (err) => {
            if (err) {
               return console.error(err);
            }

            return res.send('ok');
         });
   });
};

module.exports.edit = (req, res) => {
   if (req.body.id == undefined) {
      return res.send('not-send-year-id');
   }
   Year.findByIdAndUpdate(req.body.id, {
      name: req.body.name,
      description: req.body.description,
      reservations: {
         start: req.body.reservationsStart,
         end: req.body.reservationsEnd,
      },
      $push: {
         edits: {
            author: req.session.user._id,
            date: moment(),
         }
      },
   }, (err, year) => {
      if (err) {
         return console.error(err);
      }
      return res.redirect('/admin/years/list?status=edit-ok');
      return res.send('ok');
   });
};

module.exports.changeStatus = (req, res) => {
   if (req.body.id == undefined) {
      return res.send('not-sent-id');
   } else if (req.body.yearStatus == undefined) {
      return res.send('not-sent-status');
   }
   if (req.body.yearStatus == 'active') {
      Year.findOneAndUpdate({
         _id: req.session.year._id,
      }, {
         status: (moment().diff(req.session.year.reservations.end) >= 0 ? 'archived' : 'prepared'),
      }, (err, year) => {
         if (err) {
            return console.error(err);
         }
      });
   }
   Year.findByIdAndUpdate(req.body.id, {
      status: req.body.yearStatus,
   }, (err, year) => {
      if (err) {
         console.error(err);
      }
      return res.send('ok');
   });
};

module.exports.switch = (req, res) => {
   // This method switches the current (editing) year of user
   if (req.body.id == undefined) {
      return res.send('not-sent-id');
   }
   User.findById(req.session.user._id, (err, user) => {
      if (err) {
         return console.error(err);
      }
      if (!user.years.includes(req.body.id)) {
         return res.send('not-permissions-for-this-year');
      }
      Year.findById(req.body.id, (err, year) => {
         if (err) {
            return console.error(err);
         }
         if (!year) {
            return res.send('not-found-year-bad-id');
         }
         req.session.year = year;
         return res.send('ok');
      });
   });
};