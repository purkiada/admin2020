/**
 * Feedback controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const moment = require('moment');
moment.locale('cs');
const nodemailer = require('nodemailer');
const validator = require('validator');

/**
 * Models
 */
//const User = require('../../models/User');
const Feedback = require('../models/Feedback');

module.exports.new = (req, res) => {
    // check the shape of email
    let email = req.body.email.trim().toLowerCase();
    if (!validator.isEmail(email)) {
        res.redirect(`/feedback/?err=this-is-not-email${req.body.firstname ? `&firstname=${req.body.firstname}` : ''}${req.body.lastname ? `&lastname=${req.body.lastname}` : ''}${req.body.goOutTime ? `&goOutTim=${req.body.goOutTime}` : ''}${req.body.countOfChildren ? `&countOfChildren=${req.body.countOfChildren}` : ''}${req.body.countOfAdults ? `&countOfAdults=${req.body.countOfAdults}` : ''}#reservation`);
    }

    req.session.email = email;

    if (req.body.email && req.body.text) {
        new Feedback({
            year: req.session.year._id,
            email,
            text: req.body.text,
            date: moment(),
        }).save((err, feedback) => {
            if (err) {
                return console.error(err);
            }

            // Create a SMTP transporter object
            let transporter = nodemailer.createTransport(global.CONFIG.nodemailer.settings);

            // Message object
            let message = {
                from: CONFIG.nodemailer.sender,
                to: `${feedback.email}`,
                subject: 'Vaše zpětná vazba 💬',
                text: `Dobrý den,\n\nzde je potvrzení Vaší zpětné vazby:\n
        ID:              ${feedback._id}
        Email:           ${feedback.email}
        Zpětná vazba:    ${feedback.text}\n\nDěkujeme za Vaši zpětnou vazbu!\n\nS přáním hezkého dne,\nPořadatelé Purkiády`,
            };

            transporter.sendMail(message, (err, info, response) => {
                if (err) {
                    console.error('Error occurred. ' + err.message);
                    if (req.originalUrl != '/feedback/new') {
                        return res.redirect(`${req.originalUrl}?status-feedback=${err.name}`);
                    } else {
                        return res.redirect(`/?status-feedback=${err.name}#feedback`);
                    }
                }
                if (req.originalUrl != '/feedbacks/new') {
                    return res.redirect(`${req.originalUrl}?status-feedback=ok`);
                } else {
                    return res.redirect(`/feedback?status-feedback=ok`);
                }
                // console.log( info);
                // console.log(response);
            });
        });
    } else {
        res.redirect('/feedback?err-feedback=badForm#feedback');
    }
};

module.exports.delete = (req, res) => {
    if (req.body.id) {
        Feedback.findOneAndDelete({
            _id: req.body.id,
        }, (err, feedback) => {
            if (err) {
                return console.error(err);
            }
            res.send('ok');
        });
    } else {
        res.send('not-sent-id');
    }
};