/**
 * Village controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */

/**
 * Models
 */
const Village = require('../models/Village');

module.exports.list = (req, res) => {
    Village.find({}, (err, villages) => {
        if (err) {
            return console.error(err);
        }
        res.send(villages);
    })
};