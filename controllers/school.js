/**
 * School controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */

/**
 * Models
 */
const School = require('../models/School');

module.exports.list = (req, res) => {
    School.find({})
        .populate('village')
        .exec((err, schools) => {
            if (err) {
                return console.error(err);
            }
            res.send(schools);
        })
};


module.exports.find = (req, res) => {
    let village;
    if (req.body.village != undefined) {
        village = req.body.village;
    } else if (req.query.village != undefined) {
        village = req.query.village;
    } else {
        return res.send('not-sent-village-id');
    }
    School.find({
        village
    })
        .populate('village')
        .exec((err, schools) => {
            if (err) {
                return console.error(err);
            }
            res.send(schools);
        })
};