/**
 * Email controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const moment = require('moment');
moment.locale('cs');
const nodemailer = require("nodemailer");
var Imap = require('imap'),
    inspect = require('util').inspect;
var fs = require('fs'), fileStream;

/**
 * Models
 */
const User = require('../../models/User');
const Reservation = require('../../models/Reservation');

module.exports.sendEmail = (req, res) => {
    // Create a SMTP transporter object
    let transporter = nodemailer.createTransport(global.CONFIG.nodemailer.settings);

    if (req.body.recipient.includes(',')) {
        let recipients = req.body.recipient.split(',');
        for (let i = 0; i < recipients.length; i++) {
            recipients[i] = recipients[i].trim();
        }
    }

    // Message object
    let message = {
        from: CONFIG.nodemailer.sender,
        to: req.body.recipient.trim(),
        subject: req.body.subject.trim(),
        text: req.body.text.trim(),
    };

    transporter.sendMail(message, (err, info, response) => {
        if (err) {
            console.error('Error occurred. ' + err.message);
            return process.exit(1);
        }
        //console.log(info);
        //console.log(response);
    });

    return res.redirect('/admin/emails/write?status=email-sent')
};

module.exports.getMailbox = (req, res) => {
    // Declaring new imap object
    var imap = new Imap({
        user: req.body.email ? req.body.email : CONFIG.nodemailer.settings.auth.user, // example: aakanksha.jain8@gmail.com
        password: req.body.password ? req.body.password : CONFIG.nodemailer.settings.auth.pass, // Remember, using just password for authentication will only work if you have less secured apps enabled 
        host: req.body.host ? req.body.host : CONFIG.imap.host,
        port: req.body.port ? req.body.port : CONFIG.imap.port,
        tls: req.body.tls ? req.body.tls : CONFIG.imap.secure,
    });

    // Program to receive emails. 
    /* This pretty much contains receiving emails, deciding which parts of email to receive,
    and what do display on console after execution of program */
    function openInbox(cb) {
        imap.openBox('INBOX', true, cb);
    }
    /*
        imap.once('ready', function () {
            openInbox(function (err, box) {
                //if (err) throw err;
                if (err) {
                    res.send(err);
                    return console.error(err);
                }
    
                let f = imap.seq.fetch('1:3', {
                    bodies: 'HEADER.FIELDS (FROM TO SUBJECT DATE)',
                    struct: true,
                });
    
                f.on('message', function (msg, seqno) {
                    console.log('Message #%d', seqno);
                    var prefix = '(#' + seqno + ') ';
                    msg.on('body', function (stream, info) {
                        var buffer = '';
                        stream.on('data', function (chunk) {
                            buffer += chunk.toString('utf8');
                        });
                        stream.once('end', function () {
                            console.log(prefix + 'Parsed header: %s', inspect(Imap.parseHeader(buffer)));
                        });
                    });
                    msg.once('attributes', function (attrs) {
                        console.log(prefix + 'Attributes: %s', inspect(attrs, false, 8));
                    });
                    msg.once('end', function () {
                        console.log(prefix + 'Finished');
                    });
                });
    
                f.once('error', function (err) {
                    return console.error('Fetch error: ' + err);
                });
    
                f.once('end', function () {
                    imap.end();
                    return console.error('Done fetching all messages!');
                });
            });
        });
    */
    imap.once('ready', function () {
        openInbox(function (err, box) {
            //if (err) throw err;
            if (err) {
                return console.error(err);
            }
            // Change the date with the one from which you want receive emails
            //Unseen means you'll only get mails that are unseen
            imap.search(['UNSEEN', ['SINCE', 'June 15, 2018']], function (err, results) {
                if (err) throw err;
                var f = imap.fetch(results, { bodies: '' });
                f.on('message', function (msg, seqno) {
                    console.log('Message #%d', seqno);
                    var prefix = '(#' + seqno + ') ';
                    msg.on('body', function (stream, info) {
                        console.log(prefix + 'Body');
                        stream.pipe(fs.createWriteStream('msg-' + seqno + '-body.txt'));
                    });
                    msg.once('attributes', function (attrs) {
                        console.log(prefix + 'Attributes: %s', inspect(attrs, false, 8));
                    });
                    msg.once('end', function () {
                        console.log(prefix + 'Finished');
                    });
                });
                f.once('error', function (err) {
                    console.log('Fetch error: ' + err);
                });
                f.once('end', function () {
                    console.log('Done fetching all messages!');
                    imap.end();
                });
            });
        });
    });

    imap.once('error', function (err) {
        console.log(err);
    });

    imap.once('end', function () {
        console.log('Connection ended');
    });

    imap.connect();
    return res.send('ok');
};