/**
 * Page controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const moment = require('moment');
moment.locale('cs');

/**
 * Models
 */
const Feedback = require('../../models/Feedback');
const Reservation = require('../../models/Reservation');
const User = require('../../models/User');
const Year = require('../../models/Year');
const Student = require('../../models/Student');
const School = require('../../models/School');

/**
 * Login pages
 */
module.exports.login = (req, res) => {
    res.render('login/login', { req, res });
};

module.exports.forgotPassword = (req, res) => {
    res.render('login/forgot-password', { req, res });
};

module.exports.register = (req, res) => {
    res.render('login/register', { req, res });
};

/**
 * Error pages
 */
module.exports.error404 = (req, res) => {
    res.render('admin/errors/notFound', { req, res, active: 'error', title: '404 Nenalezeno' });
};

module.exports.error403 = (req, res) => {
    res.render('admin/errors/accessDenied', { req, res, active: 'error', title: '403 Přístup odepřen!' });
};

/**
 * Dashboard
 */
module.exports.dashboard = (req, res) => {
    Student.countDocuments({
        year: req.session.year._id,
    }, (err, countOfStudents) => {
        if (err) {
            return console.error(err);
        }
        Student.countDocuments({
            year: req.session.year._id,
            confirm: true,
        }, (err, countOfConfirmedStudents) => {
            if (err) {
                return console.error(err);
            }
            return res.render('admin/dashboard', { req, res, title: 'Přehled', active: 'dashboard', countOfStudents, countOfConfirmedStudents });
        });
    });
};

/**
 * Students
 */
module.exports.listStudents = (req, res) => {
    return res.render('admin/students/list', { req, res, active: 'students', title: 'Seznam studentů' });
};

module.exports.newStudent = (req, res) => {
    return res.render('admin/students/new', { req, res, active: 'students', title: 'Nový student' })
};

module.exports.editStudent = (req, res) => {
    Student.findById(req.query.id, (err, student) => {
        if (err) {
            return console.error(err);
        }
        return res.render('admin/students/edit', { req, res, student, active: 'students', title: 'Editace studenta' })
    });
};

module.exports.detailStudent = (req, res) => {
    Student.findById(req.query.id)
        .populate('school')
        .populate('year')
        .populate('author')
        .exec((err, student) => {
            if (err) {
                return console.error(err);
            }
            return res.render('admin/students/detail', { req, res, student, active: 'students', title: 'Detail studenta' });
        });
};

/**
 * Reservations
 */
module.exports.listReservations = (req, res) => {
    Reservation.find({
        year: req.session.year._id,
    })
        .populate('year')
        .populate('author')
        .exec((err, reservations) => {
            if (err)
                return console.error(err);
            return res.render('admin/reservations/list', { req, res, reservations, active: 'reservations', title: 'Seznam rezervací' });
        });
};

module.exports.newReservation = (req, res) => {
    return res.render('admin/reservations/new', { req, res, active: 'reservations', title: 'Nová rezervace' })
};

module.exports.editReservation = (req, res) => {
    Reservation.findById(req.query.id, (err, reservation) => {
        if (err) {
            return console.error(err);
        }
        return res.render('admin/reservations/edit', { req, res, reservation, active: 'reservations', title: 'Editace rezervace' })
    });
};

/**
 * Feedbacks
 */
module.exports.listFeedbacks = (req, res) => {
    Feedback.find({}, (err, feedbacks) => {
        if (err) {
            return console.error(err);
        }
        res.render('admin/feedbacks/list', { req, res, feedbacks, active: 'feedbacks', title: 'Seznam zpětných vazeb' });
    });
};


/**
 * Years
 */
module.exports.listYears = (req, res) => {
    Year.find({})
        .populate('author')
        .exec((err, years) => {
            if (err) {
                return console.error(err);
            }
            return res.render('admin/years/list', { req, res, years, active: 'years', title: 'Seznam ročníků' });
        });
};

module.exports.newYear = (req, res) => {
    return res.render('admin/years/new', { req, res, active: 'years', title: 'Nový ročník' });
};

module.exports.editYear = (req, res) => {
    if (req.query.id == undefined) {
        return res.send('not-send-year-id');
    }
    Year.findById(req.query.id, (err, year) => {
        if (err) {
            return console.error(err);
        }
        return res.render('admin/years/edit', { req, res, year, active: 'years', title: 'Editace ročníku' });
    });
};

/**
 * Users
 */
module.exports.listUsers = (req, res) => {
    User.find({})
        .populate('author')
        .exec((err, users) => {
            if (err)
                return console.error(err);
            return res.render('admin/users/list', { req, res, users, active: 'users', title: 'Seznam uživatelů' });
        });
};

module.exports.newUser = (req, res) => {
    Year.find({}, (err, years) => {
        if (err)
            return console.error(err);
        res.render('admin/users/new', { req, res, years, active: 'users', title: 'Nový uživatel' });
    });
};

module.exports.editUser = (req, res) => {
    if (req.query.id === undefined) {
        res.send('not-send-id');
    }
    User.findById(req.query['id']).exec((err, user) => {
        if (err)
            return console.error(err);
        Year.find({}, (err, years) => {
            if (err)
                return console.error(err);
            res.render('admin/users/edit', { req, res, user, years, active: 'users', title: 'Editace uživatele' });
        });
    });
};

/**
 * Emails
 */
module.exports.writeEmail = (req, res) => {
    return res.render('admin/email/write', { req, res, active: 'emails', title: 'Napsat email' });
};

/**
 * Profile
 */
module.exports.profile = (req, res) => {
    res.render('admin/profile', { req, res, user: req.session.user, active: 'profile', title: 'Profil uživatele' });
};