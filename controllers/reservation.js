/**
 * Reservation controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const moment = require('moment');
moment.locale('cs');
const nodemailer = require("nodemailer");
const validator = require('validator');
const Email = require('email-templates');
const path = require('path');

/**
 * Models
 */
const User = require('../models/User');
const Reservation = require('../models/Reservation');
const Year = require('../models/Year');

module.exports.new = (req, res) => {
    // save the object to the database, handle the errors
    let email = req.body.email.trim().toLowerCase();
    if (!validator.isEmail(email)) {
        res.redirect(`/reservation/?err=this-is-not-email${req.body.firstname ? `&firstname=${req.body.firstname}` : ''}${req.body.lastname ? `&lastname=${req.body.lastname}` : ''}${req.body.goOutTime ? `&goOutTim=${req.body.goOutTime}` : ''}${req.body.countOfChildren ? `&countOfChildren=${req.body.countOfChildren}` : ''}${req.body.countOfAdults ? `&countOfAdults=${req.body.countOfAdults}` : ''}#reservation`);
    }
    req.session.email = email;
    req.body.countOfChildren = parseInt(req.body.countOfChildren, 10);

    // find the reservation by email
    Reservation.findOne({
        year: req.session.year,
        email: req.session.email,
    }, (err, reservation) => {
        if (err)
            return console.error(err);

        if (reservation && reservation.email === email) {
            // reservation with inputed email found
            return res.redirect(`/reservation/?err=emailAlreadyExists${req.body.firstname ? `&firstname=${req.body.firstname}` : ''}${req.body.lastname ? `&lastname=${req.body.lastname}` : ''}${req.body.goOutTime ? `&goOutTim=${req.body.goOutTime}` : ''}${req.body.countOfChildren ? `&countOfChildren=${req.body.countOfChildren}` : ''}${req.body.countOfAdults ? `&countOfAdults=${req.body.countOfAdults}` : ''}#reservation`);
        }

        // No reservation with inputed email found

        // Create new reservation
        new Reservation({
            year: req.session.year._id,
            author: (req.session.user ? req.session.user._id : null),
            name: {
                first: req.body.firstname,
                last: req.body.lastname,
            },
            email,
            description: req.body.description,
            count: {
                kids: req.body.countOfChildren,
                adults: req.body.countOfAdults,
            },
            date: moment(),
        }).save((err, reservation) => {
            if (err) {
                return console.error(err);
            }

            // Create a SMTP transporter object
            let transporter = nodemailer.createTransport(global.CONFIG.nodemailer.settings);

            // Message object
            let message = {
                from: CONFIG.nodemailer.sender,
                to: reservation.name.first + ' ' + reservation.name.last + ' <' + reservation.email + '>',
                subject: `Vaše rezervace 📆`,
                text: `Dobrý den,\n\nzde je potvrzení Vaší rezervace:\n
            ID: ${reservation._id}
            Křestní jméno: ${reservation.name.first}
            Příjmení: ${reservation.name.last}
            Email: ${reservation.email}\n\n\nPokud budete spokojení s průběhem Purkiády, zanechte nám prosím zpětnou vazbu zde: https://purkiada.cz/feedback\n\nS přáním hezkého dne,\nPořadatelé Purkiády`,
                //html: '<h1>Dobrý den,</h1><p>zde je vaše heslo <b>' + newPassword + '</b>!</p><p>Nyní se můžete přihlásit s tímto heslem.</p><br><p>S přáním hezkého dne,<br/>Pořadatelé Pohádkového lesa v Rudici</p>'
                //html: fs.readFileSync('../mail/forgot-password.html');
            };

            transporter.sendMail(message, (err, info, response) => {
                if (err) {
                    console.error('Error occurred. ' + err.message);
                    if (req.originalUrl != '/reservations/new') {
                        return res.redirect(`${req.originalUrl}?status=${err.name}`);
                        //res.redirect('/?status=reservation-ok#reservation');
                        //res.redirect('/admin/reservations/?status=ok');
                    } else {
                        return res.redirect(`/reservation/?status=${err.name}#reservation`);
                    }
                    return process.exit(1);
                }
                // console.log( info);
                // console.log(response);
            });

            //console.log(`Successfull reservation!\n${req.body}`);
            console.log(`${moment().format('YYYY-MM-DD HH:mm:ss')} Successfull reservation! ${reservation.email}`);
            // redirect back (after the save is done)
            if (req.originalUrl != '/reservations/new') {
                if (req.originalUrl.includes('new')) {
                    req.originalUrl = req.originalUrl.split('new')[0];
                }
                return res.redirect(`${req.originalUrl}?status=ok`);
                //res.redirect('/?status=reservation-ok#reservation');
                //res.redirect('/admin/reservations/?status=ok');
            } else {
                return res.redirect('/reservation/?status=ok');
            }
        });
    });
};

module.exports.delete = (req, res) => {
    if (!req.body.id) {
        res.send('not-send-id');
    } else {
        Reservation.findByIdAndDelete(req.body.id, (err) => {
            if (err)
                console.error(err);
            res.send('ok');
        });
    }
};

module.exports.deleteAll = (req, res) => {
    Reservation.deleteMany({
        year: req.session.year,
    }, (err) => {
        if (err) {
            return console.error(err);
        } else {
            res.send('ok');
        }
    });
};

module.exports.pay = (req, res) => {
    if (req.body.id) {
        Reservation.findById(req.body.id, (err, reservation) => {
            if (err) {
                res.send(err);
                return console.error(err);
            }
            Reservation.findByIdAndUpdate(req.body.id, {
                paid: !reservation.paid,
            }, (err) => {
                if (err) {
                    res.send(err);
                    return console.error(err);
                }
                //console.log(`ok-${updatedReservation.paid}`);
                return res.send(`ok-${!reservation.paid}`);
            });
        });
    } else {
        return res.send('not-sent-id');
    }
};

module.exports.sendEmail = (req, res) => {
    if (!req.body.id) {
        res.send('not-sent-id');
    } else {
        Reservation.findById(req.body.id, (err, reservation) => {
            if (err)
                console.error(err);

            // Create a SMTP transporter object
            let transporter = nodemailer.createTransport(global.CONFIG.nodemailer.settings);

            // Message object
            let message = {
                from: CONFIG.nodemailer.sender,
                to: reservation.name.first + ' ' + reservation.name.last + ' <' + reservation.email + '>',
                subject: 'Rekapitulace Vaší rezervace 📆',
                text: `Dobrý den,\n\nzde je rekapitulace Vaší rezervace:\n
            ID: ${reservation._id}
            Křestní jméno: ${reservation.name.first}
            Příjmení: ${reservation.name.last}
            Email: ${reservation.email}\n\n\nPokud budete spokojení s průběhem Purkiády, zanechte nám prosím zpětnou vazbu zde: https://purkiada.cz/feedback\n\nS přáním hezkého dne,\nPořadatelé Purkiády`,
                //html: '<h1>Dobrý den,</h1><p>zde je vaše heslo <b>' + newPassword + '</b>!</p><p>Nyní se můžete přihlásit s tímto heslem.</p><br><p>S přáním hezkého dne,<br/>Pořadatelé Pohádkového lesa v Rudici</p>'
                //html: fs.readFileSync('../mail/forgot-password.html');
            };

            transporter.sendMail(message, (err, info, response) => {
                if (err) {
                    console.log('Error occurred. ' + err.message);
                    return process.exit(1);
                }
                //console.log(info);
                //console.log(response);
            });

            res.send('ok');
        });
    }
};

module.exports.sendEmailAllReservations = (req, res) => {
    Reservation.find({
        year: req.session.year,
    }, (err, reservations) => {
        if (err) {
            res.send(err);
            return console.error(err);
        }
        if (!req.body.subject) {
            return res.send('not-send-subject');
        } else if (!req.body.text) {
            return res.send('not-send-text');
        }

        let recipients = [];
        for (let i = 0; i < reservations.length; i++) {
            recipients.push(`${reservations[i].name.first} ${reservations[i].name.last} <${reservations[i].email}>`);
        }

        let transporter = nodemailer.createTransport(global.CONFIG.nodemailer.settings);

        // Message object
        let message = {
            from: CONFIG.nodemailer.sender,
            to: recipients,
            subject: req.body.subject,
            text: req.body.text,
            //html: '<h1>Dobrý den,</h1><p>zde je vaše heslo <b>' + newPassword + '</b>!</p><p>Nyní se můžete přihlásit s tímto heslem.</p><br><p>S přáním hezkého dne,<br/>Pořadatelé Pohádkového lesa v Rudici</p>'
            //html: fs.readFileSync('../mail/forgot-password.html');
        };

        transporter.sendMail(message, (err, info, response) => {
            if (err) {
                console.error('Error occurred. ' + err.message);
                res.send(err.message);
                return process.exit(1);
            }
            //console.log(info);
            //console.log(response);
        });
        return res.send('ok');
    });
};

module.exports.changeStatus = (req, res) => {
    if (req.body.id == undefined) {
        return res.send('not-sent-id');
    } else if (req.body.reservationStatus == undefined) {
        return res.send('not-sent-status');
    }
    Reservation.findByIdAndUpdate(req.body.id, {
        status: req.body.reservationStatus,
    }, (err, reservation) => {
        if (err) {
            console.error(err);
        }
        return res.send('ok');
    });
};

module.exports.edit = (req, res) => {
    let reservation = {
        name: {
            first: req.body.firstname,
            last: req.body.lastname,
        },
        email: req.body.email,
        count: {
            kids: req.body.countOfChildren,
            adults: req.body.countOfAdults,
        },
        description: req.body.description,
    };
    Reservation.updateOne({
        _id: req.body.id,
    }, reservation, (err) => {
        if (err)
            return console.error(err);
        if (req.body.sendEmail == 'on') {
            reservation._id = req.body.id;
            //send checkout email
            // Create a SMTP transporter object
            let transporter = nodemailer.createTransport(global.CONFIG.nodemailer.settings);

            // Message object
            let message = {
                from: CONFIG.nodemailer.sender,
                to: reservation.name.first + ' ' + reservation.name.last + ' <' + reservation.email + '>',
                subject: 'Vaše změnná rezervace 📆',
                text: `Dobrý den,\n\nněkdo v admin sekci změnil vaší rezervaci, zde jsou aktuální údaje:\n
            ID: ${reservation._id}
            Křestní jméno: ${reservation.name.first}
            Příjmení: ${reservation.name.last}
            Email: ${reservation.email}\n\n\nPokud budete spokojení s průběhem Purkiády, zanechte nám prosím zpětnou vazbu zde: https://purkiada.cz/feedback\n\nS přáním hezkého dne,\nPořadatelé Purkiády`,
                //html: '<h1>Dobrý den,</h1><p>zde je vaše heslo <b>' + newPassword + '</b>!</p><p>Nyní se můžete přihlásit s tímto heslem.</p><br><p>S přáním hezkého dne,<br/>Pořadatelé Pohádkového lesa v Rudici</p>'
                //html: fs.readFileSync('../mail/forgot-password.html');
            };

            transporter.sendMail(message, (err, info, response) => {
                if (err) {
                    console.log('Error occurred. ' + err.message);
                    return process.exit(1);
                }
                //console.log(info);
                //console.log(response);
            });
        }
        res.redirect('/admin/reservations/list?status=edit-ok');
    })
};

module.exports.import = (req, res) => {
    if (req.body.sql) {
        reservationsStrings = req.body.sql.split('), (');
        reservations = [];
        setTimeout(function () {
            for (let i = 0; i < reservationsStrings.length; i++) {
                let reservation = reservationsStrings[i].split(',');
                let reservationObject = {
                    author: req.session.user._id,
                    year: req.session.year,
                    description: 'Importováno',
                    name: {
                        first: null,
                        last: null,
                    },
                    email: null,
                    time: null,
                    count: {
                        kids: null,
                        adults: null,
                    },
                };
                for (let ii = 0; ii < reservation.length; ii++) {
                    reservation[ii] = reservation[ii].trim().replace('\'', '').replace('\'', '');

                }
                //console.log(reservations[i].split(','));
                //console.log(reservations[i]);
                reservationObject.name.first = reservation[1];
                reservationObject.name.last = reservation[2];
                reservationObject.email = reservation[3].toLowerCase();
                reservationObject.time = reservation[4];
                reservationObject.count.kids = reservation[5];
                reservationObject.count.adults = reservation[6];
                //console.log(reservationObject);
                reservations.push(reservationObject);
            }
            for (let i = 0; i < reservations.length; i++) {
                /*Reservation.findOne({
                    $or: [
                        {
                            email: reservation.email,
                        },
                    ]
                }, (err, reservation) => {
                    if (err)
                        return console.error(err);
                    if (!reservation && reservation.email == email) {*/
                //return res.redirect('/?err=emailAlreadyExists#reservation');
                new Reservation(reservations[i]).save((err, reservation) => {
                    if (err) {
                        return console.log(err);
                    }
                    console.log(reservation);
                });
                /*}
            });*/
            }
            //console.log(req.body.sql);
            res.redirect('/admin/reservations/import?status=import-ok');
        }, 1000);
    }
};