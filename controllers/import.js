/**
 * Import controller
 * @author Lukas Matuska (drunk) lukasmatuska.cz lukynmatuska@gmail.com
 * @version 1.0
 */

/**
 * Libs
 */
const moment = require('moment');
moment.locale('cs');
const fs = require('fs');

/**
 * Models
 */
const School = require('../models/School');
const Village = require('../models/Village');

module.exports.tmp = (req, res) => {
    let rawdata = fs.readFileSync('static/schools.json');
    let villages = JSON.parse(rawdata);
    // console.log(Object.keys(villages));
    for (let i = 0; i < Object.keys(villages).length; i++) {
        const villageName = Object.keys(villages)[i];
        const village = {
            postalCode: '',
            name: '',
        };
        if (villageName.includes(" ")) {
            for (let ii = 0; ii < villageName.split(' ').length; ii++) {
                const element = villageName.split(' ')[ii];
                if (/\d/.test(element)) {
                    village.postalCode += village.postalCode.length == 0 ? `${element}` : ` ${element}`;
                } else {
                    village.name += village.name.length == 0 ? `${element}` : ` ${element}`;
                }
            }
        } else {
            delete village.postalCode;
            village.name = villageName;
        }
        new Village(village).save((err, village) => {
            if (err) {
                res.send(err);
                return console.error(err);
            }
            for (let j = 0; j < villages[villageName].length; j++) {
                const schoolFromJson = villages[villageName][j];
                new School({
                    village: village._id,
                    name: schoolFromJson,
                }).save((err, school) => {
                    if (err) {
                        res.send(err);
                        return console.error(err);
                    }
                });
            }
        });
    }
    res.send('ok');
    return console.log('Successfull import of villages and schools!');
};