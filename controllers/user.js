/**
 * User controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const bcrypt = require('bcrypt');
const moment = require('moment');
moment.locale('cs');
const randomstring = require('randomstring');
const nodemailer = require("nodemailer");
const fs = require('fs'); // File system

/**
 * Models
 */
const User = require('../models/User');
const Year = require('../models/Year');


/********************************
 ********** METHODS *************
 *******************************/

/**
* Login, logout and register
*/
module.exports.register = (req, res) => {
    if (req.body.firstname === undefined) {
        return res.send('not-sent-firstname');
    } else if (req.body.lastname === undefined) {
        return res.send('not-sent-lastname');
    } else if (req.body.email === undefined) {
        return res.send('not-sent-email');
    } else if (req.body.username === undefined) {
        return res.send('not-sent-username');
    } else if (req.body.password === undefined) {
        return res.send('not-sent-password');
    } else if (req.body.passwordRepeat === undefined) {
        return res.send('not-sent-passwordRepeat');
    } else if (req.body.password != req.body.passwordRepeat) {
        return res.send('passwords-doesnt-match');
    }

    let email = req.body.email.trim().toLowerCase();
    let username = req.body.username.trim().toLowerCase();
    User.findOne({
        $or: [
            {
                email,
            },
            {
                username,
            }
        ],
    }, (err, user) => {
        if (err) {
            return console.error(err);
        }

        if (user && user.username === username) {
            // exist user with that username
            return res.send('username-exist');
            //return res.redirect('/register/?err=usernameAlreadyExists');
        }

        if (user && user.email === email) {
            // exist user with that email
            return res.send('email-exist');
            //return res.redirect('/register/?err=emailAlreadyExists');
        }

        new User({
            name: {
                first: req.body.firstname,
                last: req.body.lastname,
            },
            username,
            password: bcrypt.hashSync(req.body.password, 15), // don't go under 13!
            email,
        }).save((err, user) => {
            if (err) {
                return console.error(err);
            }
            // Session email
            req.session.email = user.email;


            let transporter = nodemailer.createTransport(global.CONFIG.nodemailer.settings);
            var text = `Dobrý den,\n\n`;
            if (req.originalUrl.includes('admin')) {
                text += `někdo Vám v naší admin sekci vytvořil účet, zde jsou Vaše údaje:\n`;
            } else {
                text += `jelikož jste se u nás zaregistroval(a), tak Vám posíláme potrvzení:`;
            }
            text += `\tKřestní jméno:\t${user.name.first}\n\tPříjmení:\t${user.name.last}\n\tEmail:\t${user.email}\n\nUživatelské jméno:\t${user.username}}\nPřihlásit se můžete zde: https://admin.purkiada.cz/login\n\nS přáním hezkého dne,\nPořadatelé Purkiády`;

            // Message object
            let message = {
                from: CONFIG.nodemailer.sender,
                to: user.name.first + ' ' + user.name.last + ' <' + user.email + '>',
                subject: 'Váš nový účet 👤🔑',
                text,
            };

            transporter.sendMail(message, (err, info, response) => {
                if (err) {
                    console.log('Error occurred. ' + err.message);
                    return process.exit(1);
                }
                //console.log(info);
                //console.log(response);
            });

            return res.send('ok');
            /*if (req.originalUrl == '/admin/register') {
                res.render('admin/successfulRegister', { req, res });
            } else {
                req.session.user = saved;
                //res.redirect('/admin');
            }*/
        });
    });
};

module.exports.login = (req, res) => {
    if (req.body.username === undefined) {
        //return res.redirect('/admin/login/?err=not-sent-username')
        return res.send('not-sent-username');
    } else if (req.body.password === undefined) {
        //return res.redirect('/admin/login/?err=not-sent-password')
        return res.send('not-sent-password');
    }

    User.findOne({
        $or: [{
            username: req.body.username.trim().toLowerCase(),
        }, {
            email: req.body.username.trim().toLowerCase(),
        }],
    }, (err, user) => {
        if (err)
            return console.error(err);
        if (!user) {
            //return res.redirect('/admin/login/?status=bad-username')
            return res.send('bad-username');
        }

        // compare the passwords
        bcrypt.compare(req.body.password, user.password, (err, same) => {
            if (err)
                return console.error(err);

            if (!same) {
                if (user.rescue) {
                    return res.send('bad-password');
                } else {
                    // compare the passwords
                    bcrypt.compare(req.body.password, user.rescue.password, (err, sameRescue) => {
                        if (err)
                            return console.error(err);
                        if (!sameRescue) {
                            console.log('kdopak nam to loupe pernicek?');
                            // if the passwords are not the same, return message
                            //return res.redirect(`/admin/login?status=bad-password&username=${user.username}`);
                            return res.send('bad-password');
                        }
                    });
                }
            } else {

                User.findOneAndUpdate({
                    username: user.username,
                }, {
                    $push: {
                        logins: {
                            time: moment(),
                            ip: req.headers['x-forwarded-for'],
                        },
                    }
                })
                    .populate('author')
                    .populate('years')
                    .exec((err, user) => {
                        if (err)
                            return console.error(err);

                        if (user.years.length == 0) {
                            return res.send('null-years');
                        } else if (user.permissions.length == 0) {
                            return res.send('null-permissions');
                        }
                        let i = 0;
                        while (i != (Number(user.years.length) - 1)) {
                            if (user.years[i].status === 'active') {
                                req.session.year = user.years[i];
                            }
                            i++;
                        }
                        if (req.session.year === undefined) {
                            req.session.year = user.years[user.years.length - 1];
                        }

                        req.session.user = user;
                        //console.log(`Succefull login: ${user.username}`);
                        if (req.session.user.permissions.includes('masterAdmin'))
                            return res.send('ok-admin');
                        //return res.redirect('/admin');
                        return res.send('ok');
                        return res.redirect('/');
                    });
            }
        });
    });
};

module.exports.logout = (req, res) => {
    let redirectString;
    if (req.session.user.permissions.includes('admin')) {
        redirectString = '/admin';
    } else {
        redirectString = '';
    }
    req.session.destroy();
    return res.redirect(`${redirectString}/login`);
};

/**
 * Update and edit
 */
module.exports.update = (req, res) => {
    User.findOne({
        _id: req.session.user._id,
    }, (err, user) => {
        if (err)
            return console.error(err);
        if (req.body.password) {
            // compare the passwords
            bcrypt.compare(req.body.password, user.password, (err, same) => {
                if (err)
                    return console.error(err);

                // if the password are not the same, return message
                if (!same)
                    return res.redirect('/login?err=badPassword');

                if (req.body.firstname)
                    user.name.first = req.body.firstname;
                if (req.body.lastname)
                    user.name.last = req.body.lastname;
                if (req.body.username)
                    user.username = req.body.username;
                if (req.body.email)
                    user.email = req.body.email;
                req.session.user = user;
                user.save((err) => {
                    if (err)
                        return console.error(err);
                    res.redirect('/admin/profile?err=ok');
                });
            });
        } else {
            res.redirect('/admin/profile?err=passwordWasntProvided');
        }
    });
};

module.exports.edit = (req, res) => {
    if (req.body.id) {
        //console.log(req.body);
        if (req.body.masterAdmin == "on") {
            req.body.masterAdmin = true;
        }
        User.updateOne({
            _id: req.body.id,
        }, {
            name: {
                first: req.body.firstname,
                last: req.body.lastname,
            },
            masterAdmin: req.body.masterAdmin,
            ail: req.body.email,
            username: req.body.username,
            photoUrl: req.body.profilePhotoUrl,
            years: req.body.years,
            permissions: req.body.permissions,
        }, (err, user) => {
            if (err)
                return console.error(err);
            res.redirect('/admin/users/list/?status=user-edit-ok');
        }
        );
    }
};

/**
 * Manage passwords
 */
module.exports.changePassword = (req, res) => {
    if (req.body.oldPassword && req.body.newPassword) {
        // compare the passwords
        bcrypt.compare(req.body.oldPassword, req.session.user.password, (err, same) => {
            if (err)
                return console.error(err);

            // if the password are not the same, return message
            if (!same)
                return res.redirect('/admin/profile?err=badPassword');

            User.findOneAndUpdate({
                _id: req.session.user._id,
            }, {
                $set: {
                    password: bcrypt.hashSync(req.body.newPassword, 15),
                },
            }, {
                new: true,
            }, (err, user) => {
                if (err)
                    return console.error(err);
                req.session.user = user;
                res.redirect('/admin/profile?err=ok');
            });
        });
    } else {
        res.redirect('/admin/profile?err=passwordWasntProvided');
    }
};

module.exports.resetPassword = (req, res) => {
    User.findOne({
        $or: [{
            username: req.body.username,
        }, {
            email: req.body.email,
        }],
    }, (err, user) => {
        if (err) {
            return console.error(err);
        } else if (user) {
            let newPassword = randomstring.generate();
            user.rescue.password = bcrypt.hashSync(newPassword, 15)
            user.rescue.enabled = true;
            user.save((err) => {
                if (err)
                    return console.error(err);
                // Create a SMTP transporter object
                let transporter = nodemailer.createTransport(global.CONFIG.nodemailer.settings);

                // Message object
                let message = {
                    from: CONFIG.nodemailer.sender,
                    to: user.name.first + ' ' + user.name.last + ' <' + user.email + '>',
                    subject: 'Vaše zapomenuté heslo 🔒',
                    text: `Dobrý den,\nzde je vaše heslo: ${newPassword}\nNyní se můžete přihlásit s tímto heslem. https://admin.purkiada.cz/login\n\nS přáním hezkého dne,\nPořadatelé Purkiády`,
                };

                transporter.sendMail(message, (err, info, response) => {
                    if (err) {
                        console.log('Error occurred. ' + err.message);
                        return process.exit(1);
                    }
                    //console.log(info);
                    //console.log(response);
                });
                return res.send('ok');
            });
        } else {
            return res.send('user-not-exist');
            res.redirect('/admin/forgot-password?err=not-existing-username');
        }
    });
};

module.exports.setNewPassword = (req, res) => {
    if (!req.body.id) {
        return res.send('not-send-id');
    } else if (!req.body.newPassword) {
        return res.send('not-send-password');
    } else {
        //console.log('Setting new password!');
        User.findOne({
            _id: req.body.id,
        }, (err, user) => {
            if (err) {
                return console.error(err);
            }
            //console.log(req.body.newPassword);
            user.password = bcrypt.hashSync(req.body.newPassword, 15)
            user.save((err) => {
                if (err)
                    return console.error(err);
                // Create a SMTP transporter object
                let transporter = nodemailer.createTransport(global.CONFIG.nodemailer.settings);

                // Message object
                let message = {
                    from: CONFIG.nodemailer.sender,
                    to: user.name.first + ' ' + user.name.last + ' <' + user.email + '>',
                    subject: 'Vaše nové heslo 🔑',
                    text: 'Dobrý den,\nzde je vaše heslo: ' + req.body.newPassword + '\nNyní se můžete přihlásit s tímto heslem. https://pohles.rudickamladez.cz/login\n\nS přáním hezkého dne,\nPořadatelé Pohádkového lesa v Rudici',
                    //html: '<h1>Dobrý den,</h1><p>zde je vaše heslo <b>' + newPassword + '</b>!</p><p>Nyní se můžete přihlásit s tímto heslem.</p><br><p>S přáním hezkého dne,<br/>Pořadatelé Pohádkového lesa v Rudici</p>'
                    //html: fs.readFileSync('../mail/forgot-password.html');
                };

                transporter.sendMail(message, (err, info, response) => {
                    if (err) {
                        console.log('Error occurred. ' + err.message);
                        return process.exit(1);
                    }
                    //console.log(info);
                    //console.log(response);
                });
                //res.render('admin/forgotPasswordOk', { req, res, user });
                res.send('ok');
            });
        });
    }
};

/**
 * Profile photo
 */
module.exports.changePathOfProfilePhoto = (req, res) => {
    if (req.body.newProfilePhotoLink) {
        User.findOne({
            _id: req.session.user._id,
        }, (err, user) => {
            if (err) {
                return console.error(err);
            }
            if (req.body.newProfilePhotoLink.includes('http:')) {
                req.body.newProfilePhotoLink = req.body.newProfilePhotoLink.split('http:')[1].trim();
            } else if (req.body.newProfilePhotoLink.includes('https:')) {
                req.body.newProfilePhotoLink = req.body.newProfilePhotoLink.split('https:')[1].trim();
            }
            user.photoUrl = req.body.newProfilePhotoLink;
            req.session.user = user;
            user.save((err) => {
                if (err)
                    return console.error(err);
                res.redirect('/admin/profile/?err=newProfilePhotoLinkOk');
            });
        });
    } else {
        res.redirect('/admin/profile/?err=newProfilePhotoLinkWasntProvided');
    }
};

module.exports.changeProfilePhoto = (req, res) => {
    if (req.file) {
        User.findOne({
            _id: req.session.user._id,
        }, (err, user) => {
            if (err) {
                return console.error(err);
            }
            user.photoUrl = req.file.path.slice(6, req.file.path.length);
            req.session.user = user;
            user.save((err) => {
                if (err)
                    return console.error(err);
                res.redirect('/admin/profile/?err=newProfilePhotoOk');
            });
        });
    } else {
        res.redirect('/admin/profile/?err=newProfilePhotoWasntProvided');
    }
};

module.exports.delete = (req, res) => {
    if (req.body.id) {
        User.findByIdAndDelete(req.body.id, (err) => {
            if (err)
                return console.error(err);
            res.send('ok');
        });
    } else {
        res.send('not-sent-id');
    }
};

module.exports.updateSessions = (req, res) => {
    //setTimeout(() => {
    User.findOne(
        {
            _id: req.session.user._id,
        }, (err, user) => {
            if (err) {
                return console.error(err);
            }
            if (user) {
                //console.log(user);
                req.session.user = user;
            } else {
                //res.redirect('/admin/logout');
                return res.send('fail');
            }
        }
    );
    //}, 200);
};