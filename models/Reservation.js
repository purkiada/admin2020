/*
 * Reservation database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

// https://stackoverflow.com/questions/34892143/mongoose-behaviour-around-min-and-max-with-number
/**
 * Libs
 */
const moment = require('moment');

// library for easy database manipulations
const mongoose = require('../libs/db');

// the schema itself
var reservationSchema = new mongoose.Schema({
    description: {
        type: String,
    },
    name: {
        first: String,
        last: String,
    },
    email: String,
    count: {
        adults: Number,
        kids: Number,
    },
    date: {
        type: Date,
    },
    status: {
        type: String,
        enum: ['paid', 'unpaid', 'cancelled'],
        default: 'unpaid',
    },
    year: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Year',
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    },
});

// export
module.exports = mongoose.model('Reservation', reservationSchema, 'reservation');