/*
 * Feedback database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

// library for easy database manipulations
const mongoose = require('../libs/db');
const moment = require('moment');
moment.locale('cs');

// the schema itself
var feedbackSchema = new mongoose.Schema({
    text: String,
    email: String,
    description: String,
    status: {
        type: String,
        enum: ['read', 'unread', 'done'],
        default: 'unread',
    },
    year: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Year',
    },
    date: {
        type: Date,
    },
});

// export
module.exports = mongoose.model('Feedback', feedbackSchema, 'feedback');