/*
 * Student database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

// https://stackoverflow.com/questions/34892143/mongoose-behaviour-around-min-and-max-with-number
/**
 * Libs
 */
// library for easy database manipulations
const mongoose = require('../libs/db');

// the schema itself
var studentSchema = new mongoose.Schema({
    name: {
        first: String,
        last: String,
    },
    email: String,
    date: Date,
    username: String,
    password: String,
    ip: String,
    key: String,
    score: Number,
    confirm: {
        type: Boolean,
        default: false,
    },
    school: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'School',
    },
    year: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Year',
    },
});

// export
module.exports = mongoose.model('Student', studentSchema, 'student');