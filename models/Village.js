/*
 * Village database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

// https://stackoverflow.com/questions/34892143/mongoose-behaviour-around-min-and-max-with-number
/**
 * Libs
 */
// library for easy database manipulations
const mongoose = require('../libs/db');

// the schema itself
var villageSchema = new mongoose.Schema({
    name: String,
    postalCode: String,
});

// export
module.exports = mongoose.model('Village', villageSchema, 'village');