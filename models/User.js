/**
 * User database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
// library for easy database manipulations
const mongoose = require('../libs/db');

// the schema itself
var userSchema = new mongoose.Schema({
    name: {
        first: String,
        last: String,
    },
    username: String,
    password: String,
    rescue: {
        password: String,
        enabled: {
            type: Boolean,
            default: false,
        },
    },
    email: String,
    photoUrl: {
        type: String,
        default: "/images/users/_default.png",
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    },
    permissions: [
        {
            type: String,
            //enum: CONFIG. //necoSmart
        }
    ],
    years: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Year',
    }],
    logins: [
        {
            time: Date,
            ip: String,
        },
    ],
});

// export
module.exports = mongoose.model('User', userSchema, 'user');