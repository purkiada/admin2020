/*
 * Year database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

// library for easy database manipulations
const mongoose = require('../libs/db');

// the schema itself
var yearSchema = new mongoose.Schema({
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    },
    name: String,
    description: String,
    status: {
        type: String,
        enum: ['active', 'archived', 'prepared'],
        default: 'prepared',
    },
    reservations: {
        start: Date,
        end: Date,
    },
    /*tickets: {
        price: Number,
        count: Number,
    },*/
    created: Date,
    edits: [{
        author: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
        },
        date: Date,
    }],
});

// export
module.exports = mongoose.model('Year', yearSchema, 'year');