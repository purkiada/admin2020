/*
 * School database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

// https://stackoverflow.com/questions/34892143/mongoose-behaviour-around-min-and-max-with-number
/**
 * Libs
 */
// library for easy database manipulations
const mongoose = require('../libs/db');

// the schema itself
var schoolSchema = new mongoose.Schema({
    name: String,
    village: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Village',
    },
});

// export
module.exports = mongoose.model('School', schoolSchema, 'school');